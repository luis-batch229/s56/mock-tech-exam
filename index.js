function countLetter(letter, sentence) {
    let result = 0;
    const validLetter = 'o';
    const invalidLetter = 'abc';
    const sentence = 'The quick brown fox jumps over the lazy dog';

    function letterCounter(sentence, char) {
        const letters = sentence.toLowerCase().split('');
        let letterCount = 0;
        letters.forEach((letter, index) => {
            if (letters[index] === char) {
                letterCount++;
            }
        });

        return `there are ${letterCount} ${char} in the given sentence`;
    }
    console.log(letterCounter('big bad wolf', 'b'));



    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

}






function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const isogram = 'machine';
    const notIsogram = 'hello';
    text = text.toLowerCase();
    for (let i = 0; i < text.length; i++) {
        for (let x = i + 1; x < text.length; x++) {
            if (text[i] === text[x]) {
                return false;
            }
        }
    }
    return true;

}





// Return undefined for people aged below 13.
// Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
// Return the rounded off price for people aged 22 to 64.
// The returned value should be a string.

function purchase(age, price) {
    const price = 109.4356;
    const discountedPrice = price * 0.8;
    const roundedPrice = discountedPrice.toFixed(2);

    function discountedPrice(age) {
        if (age < 13) {
            return undefined;
        } else if (age >= 13 && age <= 21 || age >= 65) {
            return (0.8 * 100 * age).toFixed(0) + "$";
        } else {
            return (100 * age).toFixed(0) + "$";
        }
    }

}

function findHotCategories(items) {

    function findHotCategories(items) {
        const hotCategories = [];
        for (const item of items) {
            if (item.stocks === 0 && !hotCategories.includes(item.category)) {
                hotCategories.push(item.category);
            }
        }
        return hotCategories;
    }

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    function findFlyingVoters(candidateA, candidateB) {
        const votersForBoth = [];
        for (const voter of candidateA) {
            if (candidateB.includes(voter) && !votersForBoth.includes(voter)) {
                votersForBoth.push(voter);
            }
        }
        return votersForBoth;
    }



    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};